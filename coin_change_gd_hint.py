def coin_change_gd(change):
    coins = [1, 2, 4, 5, 10]
    change_stat = dict()

    for c in reversed(sorted(coins)):
        change_stat[c] = change // c
        change = change % c
        print(change_stat)

    return change_stat


if __name__ == "__main__":
    print(coin_change_gd(38))
