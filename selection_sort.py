def selectionSort(data):
    sorted_data = data
    size = len(sorted_data) - 1
    for i in range(size):
        max_data = 0
        for j in range((size - i) + 1):
            if sorted_data[j] > sorted_data[max_data]:
                max_data = j
        sorted_data[size - i], sorted_data[max_data] = (
            sorted_data[max_data],
            sorted_data[size - i],
        )
    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("enter 10 number: ").split()))
    sorted_data = selectionSort(data)

    print(sorted_data)
