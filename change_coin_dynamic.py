def coin_change_dynamic(change):
    coins = [1, 2, 4, 5, 10]
    min_coins = change

    if change in coins:
        return 1
    else:
        for i in [c for c in coins if c <= change]:
            num_coins = 1 + coin_change_dynamic(change - i)

            if num_coins < min_coins:
                min_coins = num_coins
    return min_coins


if __name__ == "__main__":
    change = coin_change_dynamic(int(input("Change = ")))
    print(change)
