def change_coin(n):
    coins = [10, 5, 4, 2, 1]
    change = []
    fraction_coin = n

    for i in coins:

        coin = fraction_coin // i
        fraction_coin = fraction_coin % i
        change.append(coin)

    return coins, change


if __name__ == "__main__":
    coins, change = change_coin(38)

    for i in range(len(coins)):
        result = f"{coins[i]} change {change[i]} "

        print(result)
