import timeit


def recursive_fib(n):
    lst = [0, 1]
    if n in lst:
        return n

    return recursive_fib(n - 1) + recursive_fib(n - 2)


def fib(n):
    f0 = 0
    f1 = 1

    for i in range(1, n):
        f2 = f0 + f1
        f0 = f1
        f1 = f2

    return f1


if __name__ == "__main__":
    print(
        "recursive_fib",
        recursive_fib(10),
        timeit.timeit("recursive_fib", globals=globals()),
    )
    print("fib", fib(10), timeit.timeit("fib", globals=globals()))
